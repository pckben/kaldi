#!/opt/local/bin/python3.2
# -*- coding: utf-8 -*-

import sys
import json
import urllib.parse
import urllib.request

# WARNING: pckben's API, please do not abuse :)
# WARNING: this code cannot be open to the public!!
GOOGLE_API_KEY = "AIzaSyA6Dw03LqKqBM4A_UJp8rq80JaSke9S0nk"
URL_BASE = "https://www.googleapis.com/language/translate/v2?"

def translate(text):
  url = URL_BASE + urllib.parse.urlencode({
    'key': GOOGLE_API_KEY,
    'source': 'ms',
    'target': 'en',
    'q': text.encode('utf-8')
  })
  #print(url)
  try:
    data = urllib.request.urlopen(url).read()
  except urllib.error.HTTPError as e:
    print("error %d: %s" %(e.code, e.reason))
    return e.code

  response_text = data.decode('utf-8')
  response = json.loads(response_text)
  if len(response['data']['translations']) > 0:
    print(response['data']['translations'][0]['translatedText'])
    return 0
  else:
    print("no translation")
    return -2

if __name__ == "__main__":
  if len(sys.argv) == 1:
    sys.exit(-1)
  sys.exit(translate(sys.argv[1]))
