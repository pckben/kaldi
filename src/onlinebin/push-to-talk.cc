// gmmbin/gmm-latgen-faster.cc

// Copyright 2009-2012  Microsoft Corporation
//                      Johns Hopkins University (author: Daniel Povey)

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.
#include "feat/feature-mfcc.h"
#include "feat/wave-reader.h"
#include "online/online-audio-source.h"
#include "online/online-feat-input.h"
#include "online/online-decodable.h"
#include "online/online-faster-decoder.h"
#include "online/onlinebin-util.h"
#include "online/online-cmn.h"
#include "online/online-vad.h"
#include "util/timer.h"
#include "decoder/lattice-faster-decoder.h"
#include "feat/feature-mfcc.h"
#include <sstream>
#include <string>

#define COLOR 1
#if COLOR
#define GREEN  "\e[32m"
#define RED    "\e[31m"
#define YELLOW "\e[33m"
#define WHITE  "\e[37m"
#define RESETCOLOR "\e[m"
#else
#define GREEN ""
#define RESETCOLOR ""
#endif

using namespace std;
using namespace kaldi;
using namespace fst;

const kaldi::int32 kSampleFreq = 16000;
// PortAudio's internal ring buffer size in bytes
const kaldi::int32 kPaRingSize = 32768;
// Report interval for PortAudio buffer overflows in number of feat. batches
const kaldi::int32 kPaReportInt = 4;


int32 vad_buffer_length_ms = 500;
int32 vad_hangover_ms = 500;
int32 batch_size = 27;
int32 frame_length_ms = 25;
int32 frame_shift_ms = 10;
float vad_onset_threshold = 400;
float vad_offset_threshold = 50;
float vad_recover_threshold = 100;

bool recording = false, online_recognizing = false;
pthread_t recording_thread, online_recognition_thread;

VectorFst<StdArc> *decode_fst = NULL;
TransitionModel trans_model;
MfccOptions mfcc_opts;
Mfcc* mfcc;
Matrix<BaseFloat> lda_transform;

bool use_speaker_transform = false;
Matrix<BaseFloat> speaker_transform;

AmDiagGmm am_gmm;
SymbolTable *word_syms = NULL;
BaseFloat acoustic_scale = 0.1;
bool allow_partial = false;
int32 right_context = 3, left_context = 3;
std::vector<int32> silence_phones;


string Translate(string text) {
  char command[65536];
  memset(command, 0, 65536);
  sprintf(command, "./gtranslate.py \"%s\"", text.c_str());
  FILE *result = popen(command, "r");
  char buffer[32768];
  memset(buffer, 0, 32768);
  fread(buffer, sizeof(char), 32768, result);
  string translated = string(buffer);
  pclose(result);
  return translated;
}


/**
 * Reads samples from OnlinePaSource and fills up
 * a OnlineVectorSource
 */
void* RecordingThread(void* args) {
  // using namespace std;

  Vector<BaseFloat> *wave = reinterpret_cast<Vector<BaseFloat>*>(args);

  OnlinePaSource audio_src(kSampleFreq, kPaRingSize, kPaReportInt);
  Vector<BaseFloat> buffer(400);
  int num_samples = 0; // number of samples recorded so far

  while (recording) {
    audio_src.Read(&buffer, NULL);
    if (wave->Dim() == 0) {
      wave->Resize(buffer.Dim());
    }
    if (num_samples + buffer.Dim() > wave->Dim()) {
      int new_length = wave->Dim() * 2;
      wave->Resize(new_length, kCopyData);
    }
    SubVector<BaseFloat> wave_ptr(*wave, num_samples, buffer.Dim());
    wave_ptr.CopyFromVec(buffer);
    num_samples += buffer.Dim();
  }
  wave->Resize(num_samples, kCopyData);
  return NULL;
}



void StartRecording(Vector<BaseFloat> *wave) {
  recording = true;
  pthread_create(&recording_thread, NULL, RecordingThread, wave);
}
void StopRecording() {
  recording = false;
  pthread_join(recording_thread, NULL);
}


void LdaTransform(Matrix<BaseFloat> &cmvn,
    Matrix<BaseFloat> &output,
    Matrix<BaseFloat> &lda_transform,
    kaldi::uint32 left_context, kaldi::uint32 right_context) {

  uint32 window_size = left_context + right_context + 1;
  uint32 window_center = left_context;
  int32 cur_feat = window_size - window_center - 1;
  uint32 trans_rows = lda_transform.NumRows();

  int32 feat_dim = cmvn.NumCols();
  Matrix<BaseFloat> feat_window;
  feat_window.Resize(window_size, feat_dim, kUndefined);
  int32 i;
  for (i = 0; i <= window_center; ++i)
    feat_window.CopyRowFromVec(cmvn.Row(0), i);
  for (; i < window_size; ++i)
    feat_window.CopyRowFromVec(cmvn.Row(i - window_center - 1), i);

  int32 window_pos = 0;

  Matrix<BaseFloat> spliced_feats;
  spliced_feats.Resize(cmvn.NumRows() - cur_feat,
      feat_dim * window_size,
      kUndefined);
  output.Resize(cmvn.NumRows() - cur_feat, trans_rows, kUndefined);

  int32 sfi = 0; // index of the current row in spliced_feats_
  for (; cur_feat < cmvn.NumRows(); ++cur_feat, ++sfi) {
    // append at the tail of the window and advance the effective window position
    feat_window.CopyRowFromVec(cmvn.Row(cur_feat), window_pos);
    window_pos = (window_pos + 1) % window_size;
    // splice the feature vectors in the current feature window
    int32 fwi = window_pos; // the oldest vector in feature window
    SubVector<BaseFloat> spliced_row(spliced_feats, sfi);
    for (int32 i = 0; i < window_size; ++i) {
      SubVector<BaseFloat> dst(spliced_row, i*feat_dim, feat_dim);
      dst.CopyRowFromMat(feat_window, fwi);
      fwi = (fwi + 1) % window_size;
    }
  }
  KALDI_VLOG(3) << "OnlineLdaInput::Compute AddMatMat";
  output.AddMatMat(1.0, spliced_feats, kNoTrans, lda_transform, kTrans, 0.0);

}

///////////////

string GetPartialResult(const std::vector<int32>& words,
    const fst::SymbolTable *word_syms) {
  KALDI_ASSERT(word_syms != NULL);
  string result = "";
  for (size_t i = 0; i < words.size(); i++) {
    string word = word_syms->Find(words[i]);
    if (word == "")
      KALDI_ERR << "Word-id " << words[i] <<" not in symbol table.";
    result += word + " ";
  }
  return result;
}

void StartOnlineDecoder(OnlineAudioSource* audioSource, FeatureExtractor* featureExtractor) {
  OnlineFasterDecoderOpts config;
  string silence_phones_str = "1:2:3:4:5:6:7:8:9:10:11:12:13:14:15";
  if (!SplitStringToIntegers(silence_phones_str, ":", false, &silence_phones))
    KALDI_ERR << "Invalid silence-phones string " << silence_phones_str;
  if (silence_phones.empty())
    KALDI_ERR << "No silence phones given!";

  VectorFst<LatticeArc> out_fst;

  OnlineFasterDecoder decoder(*decode_fst, config,
      silence_phones, trans_model);

  int frame_length_samples = frame_length_ms * (kSampleFreq/1000);
  int frame_shift_samples = frame_shift_ms * (kSampleFreq/1000);
  int vad_hangover_samples = vad_hangover_ms * (kSampleFreq/1000);
  int vad_hangover_frames = (vad_hangover_samples - frame_length_samples) / frame_shift_samples + 1;

  SimpleEnergyVad vad(vad_onset_threshold, vad_offset_threshold, 
      vad_recover_threshold, vad_hangover_frames);

  OnlineVadFeInput vadMfccInput(audioSource, featureExtractor, &vad,
      frame_length_samples,
      frame_shift_samples,
      vad_buffer_length_ms * (kSampleFreq / 1000));

  OnlineCmvnInput cmvn_input(&vadMfccInput, mfcc_opts.num_ceps, 600);
  OnlineLdaInput lda_input(&cmvn_input, mfcc_opts.num_ceps, lda_transform,
      left_context, right_context);

  OnlineFeatInputItf *decoder_input;
  OnlineSpeakerTransformInput *speaker_transform_input = NULL;
  if (use_speaker_transform) {
	  speaker_transform_input = new OnlineSpeakerTransformInput(&lda_input, speaker_transform);
	  decoder_input = speaker_transform_input;
  } else {
	  decoder_input = &lda_input;
  }
  int32 feat_dim = lda_transform.NumRows();
  OnlineDecodableDiagGmmScaled decodable(decoder_input, am_gmm, trans_model,
      acoustic_scale, batch_size,
      feat_dim, -1);

  // setup VAD-->Decoder event notification
  OnlineFasterDecoderVadListener vadListener(&decoder);
  vadMfccInput.AddVadListener(&vadListener);

  online_recognizing = true;

  OnlineFasterDecoder::DecodeState dstate;
  string utterance = "";
  while (true) {
    dstate = decoder.Decode(&decodable);

    std::cout << GREEN;
    if (dstate & (decoder.kEndUtt | decoder.kEndFeats)) {
      std::vector<int32> word_ids;
      decoder.FinishTraceBack(&out_fst);
      fst::GetLinearSymbolSequence(out_fst,
          static_cast<vector<int32> *>(0), &word_ids,
          static_cast<LatticeArc::Weight*>(0));
      string result = GetPartialResult(word_ids, word_syms);
      cout << result << endl;

      utterance += result;
      cout << RED;
      cout << Translate(utterance);
      cout << GREEN << endl;
      utterance = "";
    } else if (dstate & (decoder.kEndBatch)) {
      std::vector<int32> word_ids;
      if (decoder.PartialTraceback(&out_fst)) {
        fst::GetLinearSymbolSequence(out_fst,
            static_cast<vector<int32> *>(0), &word_ids,
            static_cast<LatticeArc::Weight*>(0));
        string result = GetPartialResult(word_ids, word_syms);
        cout << result;
        utterance += result;
      }
    }
    std::cout << RESETCOLOR;


    if (dstate == decoder.kEndFeats)
      std::cerr << "*";
    else if (dstate == decoder.kEndBatch) {
    } else if (dstate == decoder.kEndUtt) {
      //      std::cerr << "\n";
    } else
      std::cerr << "?";



    if (!online_recognizing || (dstate == decoder.kEndFeats))
      break;
  }

  if (speaker_transform_input) {
	  delete speaker_transform_input;
  }
}

void* OnlineRecognitionThread(void* args) {
  OnlinePaSource au_src(kSampleFreq, kPaRingSize, kPaReportInt);
  cout << "Press ENTER to stop." << endl;
  StartOnlineDecoder(&au_src, mfcc);
  return NULL;
}

void StartOnlineRecognition() {
  online_recognizing = true;
  pthread_create(&online_recognition_thread, NULL, OnlineRecognitionThread, NULL);
}

void StopOnlineRecognition() {
  online_recognizing = false;
  pthread_join(online_recognition_thread, NULL);
}

bool exists(const string& filename) {
  FILE* fp = fopen(filename.c_str(), "r");
  if (fp) {
    fclose(fp);
    return true;
  } else {
    return false;
  }
}

kaldi::Timer timer;
void tic() {
  timer.Reset();
}

double toc() {
  return timer.Elapsed();
}

void wait_once()
{
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  std::string s;
  std::getline(std::cin, s);
}

void SaveSettings() {
  FILE* fout = fopen("settings.ini", "w");
  fprintf(fout, "onset_threshold = %.10f\n", vad_onset_threshold);
  fprintf(fout, "offset_threshold = %.10f\n", vad_offset_threshold);
  fprintf(fout, "recover_threshold = %.10f\n", vad_recover_threshold);
  fclose(fout);
}

void ReadSettings() {
  ifstream fin;
  fin.open("settings.ini");
  if (!fin.good()) {
    SaveSettings();
    return;
  }

  string line;
  string key, delim;
  while (!fin.eof()) {
    fin >> key;
    fin >> delim;
    assert(delim == "=");
    if (key == "onset_threshold") 
      fin >> vad_onset_threshold;
    else if (key == "offset_threshold")
      fin >> vad_offset_threshold;
    else if (key == "recover_threshold")
      fin >> vad_recover_threshold;
  }
  fin.close();
}

int main(int argc, char *argv[]) {
  try {
    const char *usage =
      "Generate lattices using GMM-based model.\n"
      "Usage: gmm-latgen-faster [options] model-in (fst-in|fsts-rspecifier) features-rspecifier"
      " lda-matrix\n";
    ParseOptions po(usage);
    Timer timer;

    LatticeFasterDecoderConfig config;

    std::string word_syms_filename;
    config.Register(&po);
    po.Register("acoustic-scale", &acoustic_scale,
        "Scaling factor for acoustic likelihoods");

    po.Register("word-symbol-table", &word_syms_filename,
        "Symbol table for words [for debug output]");
    po.Register("allow-partial", &allow_partial,
        "If true, produce output even if end state was not reached.");
    po.Register("right-context", &right_context,
        "right context length for frame splicing (default=3)");
    po.Register("left-context",&left_context,
        "left context length for frame splicing (default=3)");

    string speaker_transform_filename;
    po.Register("speaker-transform", &speaker_transform_filename, "Speaker transform");

    po.Read(argc, argv);

    if (po.NumArgs() < 3 || po.NumArgs() > 5) {
      po.PrintUsage();
      exit(1);
    }

    string model_in_filename = po.GetArg(1);
    string fst_in_str = po.GetArg(2);
    // string feature_rspecifier = po.GetArg(3);
    string lda_mat_rspecifier = po.GetArg(3);

    //    system("clear");
    cout << "Loading..." << endl;

    // Load LDA transform matrix
    if (lda_mat_rspecifier != "") {
      cout << "Loading LDA transform from " << lda_mat_rspecifier << "...";
      bool binary_in;
      Input ki(lda_mat_rspecifier, &binary_in);
      lda_transform.Read(ki.Stream(), binary_in);
  	  cout << " [OK]\n";
    }

    {
      cout << "Loading am_gmm model from " << model_in_filename << "...";
      bool binary;
      Input ki(model_in_filename, &binary);
      trans_model.Read(ki.Stream(), binary);
      am_gmm.Read(ki.Stream(), binary);
  	  cout << " [OK]\n";
    }

    if (word_syms_filename != "")
      if (!(word_syms = fst::SymbolTable::ReadText(word_syms_filename)))
        KALDI_ERR << "Could not read symbol table from file "
          << word_syms_filename;

    if (speaker_transform_filename != "") {
    	use_speaker_transform = true;
    	cout << "Loading speaker transform from " << speaker_transform_filename << "...";
    	bool binary_in;
    	Input ki(speaker_transform_filename, &binary_in);
    	speaker_transform.Read(ki.Stream(), binary_in);
    	cout << " [OK]\n";
    }

    if (ClassifyRspecifier(fst_in_str, NULL, NULL) == kNoRspecifier) {
      // SequentialBaseFloatMatrixReader feature_reader(feature_rspecifier);
      // Input FST is just one FST, not a table of FSTs.

      std::ifstream is(fst_in_str.c_str(), std::ifstream::binary);
      if (!is.good())
        KALDI_ERR << "Could not open decoding-graph FST " << fst_in_str;
        cout << "Loading FST from " << fst_in_str << "...";
      	decode_fst = VectorFst<StdArc>::Read(is, fst::FstReadOptions(fst_in_str));
    	cout << " [OK]\n";
      if (decode_fst == NULL) { // fst code will warn.
        KALDI_ERR <<"load fst " <<fst_in_str <<" failed";
      } 


      MfccOptions mfcc_opts;
      mfcc_opts.use_energy = false;
      mfcc_opts.vtln_warp = 1.0;
      mfcc_opts.frame_opts.frame_length_ms = frame_length_ms;
      mfcc_opts.frame_opts.frame_shift_ms = frame_shift_ms;

      mfcc = new Mfcc(mfcc_opts);

      LatticeFasterDecoder decoder(*decode_fst, config);
      VectorFst<LatticeArc> out_fst;

      Vector<BaseFloat> wave; // TODO

      //      system("clear");
      float duration;
      ReadSettings();

      while (true) {
        cout << "\n\nOptions: " << endl;
        cout << "\t[1] Push-to-talk demo" << endl;
        cout << "\t[2] Load a .WAV file" << endl;
        cout << "\t[3] Online recognition" << endl;
        cout << "\t[4] Online recognition from .WAV file" << endl;
        cout << "\t[8] Set speaker transform" << endl;
        cout << "\t[9] Settings" << endl;
        cout << "\n\t[0] Quit" << endl;
        int choice;
        cin >> choice;
        //        system("clear");

        if (choice == 0)
          break;

        if (choice == 9) {
          cout << "Onset threshold: [" << vad_onset_threshold << "] ";
          cin >> vad_onset_threshold;
          cout << "Offset threshold: [" << vad_offset_threshold << "] ";
          cin >> vad_offset_threshold;
          cout << "Recover threshold: [" << vad_recover_threshold << "] ";
          cin >> vad_recover_threshold;
          SaveSettings();
        }
        else if (choice == 8) {
        	cout << "Transform matrix file (set to 'none' to remove): ";
        	cin >> speaker_transform_filename;
        	if (speaker_transform_filename.compare("none") == 0) {
        		cout << "Speaker transform is set to [none].\n";
        		use_speaker_transform = false;
        	} else {
        		use_speaker_transform = true;
            	cout << "Loading speaker transform from " << speaker_transform_filename << "...";
            	bool binary_in;
            	Input ki(speaker_transform_filename, &binary_in);
            	speaker_transform.Read(ki.Stream(), binary_in);
            	cout << " [OK]\n";
        	}
        }

        if (choice == 1) {
          StartRecording(&wave);
          cout << "Press ENTER to stop recording." << endl;
          wait_once();
          StopRecording();

          // save recorded audio
          int16* buf = new int16[wave.Dim()];
          for (int i=0; i<wave.Dim(); i++)
            buf[i] = (int16)wave.Data()[i];
          FILE* fout = fopen("temp.raw", "w");
          fwrite(buf, sizeof(int16), wave.Dim(), fout);
          fclose(fout);
          delete [] buf;
        }
        else if (choice == 2 || choice == 4) {
          string filename;
          do {
            cout << "Filename: ";
            cin >> filename;
            if (!exists(filename))
              cout << "File not exists.\n";
          } while (!exists(filename));

          Input inp(filename);
          WaveHolder holder;
          holder.Read(inp.Stream());
          wave.Resize(holder.Value().Data().NumCols());
          wave.CopyFromVec(holder.Value().Data().Row(0));
        }

        if (choice == 3) {
          StartOnlineRecognition();
          wait_once();
          StopOnlineRecognition();
        }

        if (choice == 1 || choice == 2 || choice == 4) {
          duration = wave.Dim() / float(kSampleFreq);
          cout << "Input duration:\t" << duration << " seconds." << endl;
        }

        if (choice == 4) {
          OnlineVectorSource onlineWavSource(wave);
          StartOnlineDecoder(&onlineWavSource, mfcc);
        }

        if ( choice == 1 || choice == 2) {
          tic();
          Matrix<BaseFloat> mfcc_output;
          mfcc->Compute(wave, &mfcc_output, NULL);

          const int32 feat_dim = mfcc_opts.num_ceps;
          const int32 cmvn_window = 600;
          OnlineCMN cmvn(feat_dim,cmvn_window);
          Matrix<BaseFloat> cmvn_output;
          cmvn.ApplyCmvn2(mfcc_output, &cmvn_output);

          Matrix<BaseFloat> lda_output;
          LdaTransform(cmvn_output, lda_output, lda_transform, left_context, right_context);

          if (use_speaker_transform) {
        	  cout << "lda_output: [" << lda_output.NumRows() << ", " << lda_output.NumCols() << "]\n";
        	  cout << "speaker transform: [" << speaker_transform.NumRows() << ", " << speaker_transform.NumCols() << "]\n";

              Matrix<BaseFloat> feat_out(lda_output.NumRows(), speaker_transform.NumRows());
              // append the implicit 1.0 to the input features.
              SubMatrix<BaseFloat> linear_part(speaker_transform, 0, speaker_transform.NumRows(), 0, lda_output.NumCols());
              cout << "linear_part: [" << linear_part.NumRows() << ", " << linear_part.NumCols() << "]\n";

              feat_out.AddMatMat(1.0, lda_output, kNoTrans, linear_part, kTrans, 0.0);
        	  cout << "feat_out: [" << feat_out.NumRows() << ", " << feat_out.NumCols() << "]\n";

              Vector<BaseFloat> offset(speaker_transform.NumRows());
              offset.CopyColFromMat(speaker_transform, lda_output.NumCols());
              feat_out.AddVecToRows(1.0, offset);

        	  cout << "feat_out: [" << feat_out.NumRows() << ", " << feat_out.NumCols() << "]\n";

        	  DecodableAmDiagGmmScaled decodable(am_gmm, trans_model, feat_out, acoustic_scale);
        	  decoder.Decode(&decodable);
          } else {
        	  DecodableAmDiagGmmScaled decodable(am_gmm, trans_model, lda_output, acoustic_scale);
        	  decoder.Decode(&decodable);
          }
          if (!decoder.GetBestPath(&out_fst)) {
            KALDI_ERR << "Failed to get traceback";
          }
          vector<int32> word_ids;
          GetLinearSymbolSequence(out_fst,
              static_cast<vector<int32> *>(0),
              &word_ids,
              static_cast<LatticeArc::Weight*>(0));

          double elapsed = toc();
          printf("Time taken:\t\t %f seconds.\n", elapsed);
          printf("Real-time factor:\t %f.\n", elapsed/duration);

          string utterance = GetPartialResult(word_ids, word_syms);
          cout << GREEN << utterance << endl << RED;
          cout << Translate(utterance) << endl;
          cout << RESETCOLOR;
        }
      }

      delete decode_fst; // delete this only after decoder goes out of scope.
    }
    delete mfcc;
    if (word_syms)
      delete word_syms;

  } catch (const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}
