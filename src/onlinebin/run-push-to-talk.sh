#gdb --args push-to-talk   \
	#--verbose=3 \
./push-to-talk   \
	--max-active=5000 --beam=12.0 --left-context=3 --right-context=3 \
	--allow-partial=true \
	--acoustic-scale=0.08  \
	--word-symbol-table=/media/demo_updated/tri2b/graph_tg222pr/words.txt \
	/media/demo_updated/tri2b/final.mdl  \
	/media/demo_updated/tri2b/graph_tg222pr/HCLG.fst \
	/media/demo_updated/tri2b/final.mat $1

