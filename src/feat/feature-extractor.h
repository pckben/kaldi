/*
 * feature-extractor.h
 *
 *  Created on: Oct 19, 2012
 *      Author: Ben
 */

#ifndef FEATURE_EXTRACTOR_H_
#define FEATURE_EXTRACTOR_H_

#include "feat/feature-functions.h"

namespace kaldi {

class FeatureExtractor {
	public:
		virtual void Compute(const VectorBase<BaseFloat> &wave,
						   Matrix<BaseFloat> *output,
						   Vector<BaseFloat> *wave_remainder = NULL) = 0;

		virtual ~FeatureExtractor() { }
};

}



#endif /* FEATURE_EXTRACTOR_H_ */
