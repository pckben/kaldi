#!/bin/bash -v

# This is not finished!

. ./cmd.sh ## You'll want to change cmd.sh to something that will work on your system.
           ## This relates to the queue.

# This is a shell script, but it's recommended that you run the commands one by
# one by copying and pasting into the shell.

#wsj0=/ais/gobi2/speech/WSJ/csr_?_senn_d?
#wsj1=/ais/gobi2/speech/WSJ/csr_senn_d?

#wsj0=/mnt/matylda2/data/WSJ0
#wsj1=/mnt/matylda2/data/WSJ1

wsj0=/data/corpora0/LDC93S6B
wsj1=/data/corpora0/LDC94S13B

#wsj0=/export/corpora5/LDC/LDC93S6B
#wsj1=/export/corpora5/LDC/LDC94S13B

local/wsj_data_prep.sh $wsj0/??-{?,??}.? $wsj1/??-{?,??}.?  || exit 1;

# Sometimes, we have seen WSJ distributions that do not have subdirectories 
# like '11-13.1', but instead have 'doc', 'si_et_05', etc. directly under the 
# wsj0 or wsj1 directories. In such cases, try something like the following:
#
# corpus=/exports/work/inf_hcrc_cstr_general/corpora/wsj
# local/cstr_wsj_data_prep.sh $corpus
#
# $corpus must contain a 'wsj0' and a 'wsj1' subdirectory for this to work.

local/wsj_prepare_dict.sh || exit 1;

utils/prepare_lang.sh data/local/dict "<SPOKEN_NOISE>" data/local/lang_tmp data/lang || exit 1;

local/wsj_format_data.sh || exit 1;


 # We suggest to run the next three commands in the background,
 # as they are not a precondition for the system building and
 # most of the tests: these commands build a dictionary
 # containing many of the OOVs in the WSJ LM training data,
 # and an LM trained directly on that data (i.e. not just
 # copying the arpa files from the disks from LDC).
 # Caution: the commands below will only work if $decode_cmd 
 # is setup to use qsub.  Else, just remove the --cmd option.
 # NOTE: If you have a setup corresponding to the cstr_wsj_data_prep.sh style,
 # use local/cstr_wsj_extend_dict.sh $corpus/wsj1/doc/ instead.

  (
   local/wsj_extend_dict.sh $wsj1/13-32.1  && \
    utils/prepare_lang.sh data/local/dict_larger "<SPOKEN_NOISE>" data/local/lang_larger data/lang_bd && \
   local/wsj_train_lms.sh && \
   local/wsj_format_local_lms.sh
  )&
 # Note: I am commenting out the commands below.  They take up a lot
 # of CPU time and are not really part of the "main recipe."
 # Be careful: appending things like "-l mem_free=10G" to $decode_cmd
 # won't always work, it depends what $decode_cmd is.
 # Note: before running this we'd have to wait for the commands above that we ran with "&":
 #   (  local/wsj_train_rnnlms.sh --cmd "$decode_cmd -l mem_free=10G" data/local/rnnlm.h30.voc10k &
 #       sleep 20; # wait till tools compiled.
 #     local/wsj_train_rnnlms.sh --cmd "$decode_cmd -l mem_free=12G" \
 #      --hidden 100 --nwords 20000 --class 350 --direct 1500 data/local/rnnlm.h100.voc20k &
 #     local/wsj_train_rnnlms.sh --cmd "$decode_cmd -l mem_free=14G" \
 #      --hidden 200 --nwords 30000 --class 350 --direct 1500 data/local/rnnlm.h200.voc30k &
 #     local/wsj_train_rnnlms.sh --cmd "$decode_cmd -l mem_free=16G" \
 #      --hidden 300 --nwords 40000 --class 400 --direct 2000 data/local/rnnlm.h300.voc40k &
 #   ) &


# Now make MFCC features.  mfccdir should be some place with a largish disk
# where you want to store MFCC features.  At this point we don't compute CMN
# stats-- we need a speech-silence model first as we're going to do special
# "balanced" cepstral mean normalization.
mfccdir=mfcc
for x in test_{eval92,eval93,dev93}{,_5k} train_si284; do 
 steps/make_mfcc.sh --cmd "$train_cmd" --nj 20 \
   data/$x exp/make_mfcc/$x $mfccdir || exit 1;
done


utils/subset_data_dir.sh --first data/train_si284 7138 data/train_si84 || exit 1

# Now make subset with the shortest 2k utterances from si-84.
utils/subset_data_dir.sh --shortest data/train_si84 2000 data/train_si84_2kshort || exit 1;

# Now make subset with half of the data from si-84.
utils/subset_data_dir.sh data/train_si84 3500 data/train_si84_half || exit 1;


# Note: the --boost-silence option should probably be omitted by default
# for normal setups.  It doesn't always help. [it's to discourage non-silence
# models from modeling silence.]
steps/train_mono.sh --boost-silence 1.25 --nj 10 --cmd "$train_cmd" \
  data/train_si84_2kshort data/lang exp/mono || exit 1;


(
 utils/mkgraph.sh --mono data/lang_test_tgpr exp/mono exp/mono/graph_tgpr && \
 steps/decode_deltas.sh --nj 10 --cmd "$decode_cmd" \
      exp/mono/graph_tgpr data/test_dev93 exp/mono/decode_tgpr_dev93 && \
 steps/decode_deltas.sh --nj 8 --cmd "$decode_cmd" \
   exp/mono/graph_tgpr data/test_eval92 exp/mono/decode_tgpr_eval92 
) &

steps/align_deltas.sh --boost-silence 1.25 --nj 10 --cmd "$train_cmd" \
   data/train_si84_half data/lang exp/mono exp/mono_ali || exit 1;

steps/train_deltas.sh --boost-silence 1.25 --cmd "$train_cmd" \
    2000 10000 data/train_si84_half data/lang exp/mono_ali exp/tri1 || exit 1;

while [ ! -f data/lang_test_tgpr/tmp/LG.fst ] || \
   [ -z data/lang_test_tgpr/tmp/LG.fst ]; do
  sleep 20;
done
sleep 30;
# or the mono mkgraph.sh might be writing 
# data/lang_test_tgpr/tmp/LG.fst which will cause this to fail.

utils/mkgraph.sh data/lang_test_tgpr exp/tri1 exp/tri1/graph_tgpr || exit 1;


steps/decode_deltas.sh --nj 10 --cmd "$decode_cmd" \
  exp/tri1/graph_tgpr data/test_dev93 exp/tri1/decode_tgpr_dev93 || exit 1;
steps/decode_deltas.sh --nj 8 --cmd "$decode_cmd" \
  exp/tri1/graph_tgpr data/test_eval92 exp/tri1/decode_tgpr_eval92 || exit 1;


# test various modes of LM rescoring (4 is the default one).
# This is just confirming they're equivalent.
for mode in 1 2 3 4; do
steps/lmrescore.sh --mode $mode --cmd "$decode_cmd" data/lang_test_{tgpr,tg} \
  data/test_dev93 exp/tri1/decode_tgpr_dev93 exp/tri1/decode_tgpr_dev93_tg$mode  || exit 1;
done

# demonstrate how to get lattices that are "word-aligned" (arcs coincide with
# words, with boundaries in the right place).
sil_label=`cat data/lang/phones/optional_silence.int`
steps/word_align_lattices.sh --cmd "$train_cmd" --silence-label $sil_label \
  data/lang_test_tgpr exp/tri1/decode_tgpr_dev93 exp/tri1/decode_tgpr_dev93_aligned || exit 1;


# Align tri1 system with si84 data.
steps/align_deltas.sh --nj 10 --cmd "$train_cmd" \
  data/train_si84 data/lang exp/tri1 exp/tri1_ali_si84 || exit 1;


# Train tri2-- a second round of training a system on delta + delta-delta features.
# We'll use this alignment to train the CMN models.
steps/train_deltas.sh --cmd "$train_cmd" \
  2500 15000 data/train_si84 data/lang exp/tri1_ali_si84 exp/tri2 || exit 1;

utils/mkgraph.sh data/lang_test_tgpr exp/tri2 exp/tri2/graph_tgpr || exit 1;

steps/decode_deltas.sh --nj 10 --cmd "$decode_cmd" \
  exp/tri2/graph_tgpr data/test_dev93 exp/tri2/decode_tgpr_dev93 || exit 1;
steps/decode_deltas.sh --nj 8 --cmd "$decode_cmd" \
  exp/tri2/graph_tgpr data/test_eval92 exp/tri2/decode_tgpr_eval92 || exit 1;

# we use models for the cepstral mean normalization to help us                                                                             
# normalize the stats to a constant proportion of silence.                                                                                 
steps/train_cmvn_models.sh --cmd "$train_cmd" data/train_si84 data/lang \
   exp/tri2 exp/tri2_cmvn

for d in data/{train_*,test_*}; do
  steps/compute_cmvn_stats_balanced.sh --cmd "$train_cmd" \
    $d exp/tri2_cmn mfcc/ exp/tri2_cmvn_`basename $d`
done

# tri2 was trained on the same subset (si84) so we can use
# that directory's alignments.
# tri3 is our first CMN+LDA+MLLT system.
steps/train_lda_mllt.sh --cmd "$train_cmd" \
   2500 15000 data/train_si84 data/lang exp/tri2 exp/tri3 || exit 1;

utils/mkgraph.sh data/lang_test_tgpr exp/tri3 exp/tri3/graph_tgpr || exit 1;
steps/decode.sh --nj 10 --cmd "$decode_cmd" \
  exp/tri3/graph_tgpr data/test_dev93 exp/tri3/decode_tgpr_dev93 || exit 1;
steps/decode.sh --nj 8 --cmd "$decode_cmd" \
  exp/tri3/graph_tgpr data/test_eval92 exp/tri3/decode_tgpr_eval92 || exit 1;

# Now, with dev93, compare lattice rescoring with biglm decoding,
# going from tgpr to tg.  Note: results are not the same, even though they should
# be, and I believe this is due to the beams not being wide enough.  The pruning
# seems to be a bit too narrow in the current scripts (got at least 0.7% absolute
# improvement from loosening beams from their current values).

steps/decode_biglm.sh --nj 10 --cmd "$decode_cmd" \
  exp/tri3/graph_tgpr data/lang_test_{tgpr,tg}/G.fst \
  data/test_dev93 exp/tri3/decode_tgpr_dev93_tg_biglm

# baseline via LM rescoring of lattices.
steps/lmrescore.sh --cmd "$decode_cmd" data/lang_test_tgpr/ data/lang_test_tg/ \
  data/test_dev93 exp/tri3/decode_tgpr_dev93 exp/tri3/decode_tgpr_dev93_tg || exit 1;

# Trying Minimum Bayes Risk decoding (like Confusion Network decoding):
mkdir exp/tri3/decode_tgpr_dev93_tg_mbr 
cp exp/tri3/decode_tgpr_dev93_tg/lat.*.gz exp/tri3/decode_tgpr_dev93_tg_mbr 
local/score_mbr.sh --cmd "$decode_cmd" \
 data/test_dev93/ data/lang_test_tgpr/ exp/tri3/decode_tgpr_dev93_tg_mbr

steps/decode_fromlats.sh --cmd "$decode_cmd" \
  data/test_dev93 data/lang_test_tgpr exp/tri2/decode_tgpr_dev93 \
  exp/tri3/decode_tgpr_dev93_fromlats || exit 1;


# Align tri3 system with si84 data.
steps/align_si.sh  --nj 10 --cmd "$train_cmd" \
  --use-graphs true data/train_si84 data/lang exp/tri3 exp/tri3_ali_si84  || exit 1;


local/run_mmi_tri3.sh


# From tri3 system, train tri4 which is LDA + MLLT + SAT.
steps/train_sat.sh --cmd "$train_cmd" \
  2500 15000 data/train_si84 data/lang exp/tri3_ali_si84 exp/tri4 || exit 1;
utils/mkgraph.sh data/lang_test_tgpr exp/tri4 exp/tri4/graph_tgpr || exit 1;
steps/decode_fmllr.sh --nj 10 --cmd "$decode_cmd" \
  exp/tri4/graph_tgpr data/test_dev93 exp/tri4/decode_tgpr_dev93 || exit 1;
steps/decode_fmllr.sh --nj 8 --cmd "$decode_cmd" \
  exp/tri4/graph_tgpr data/test_eval92 exp/tri4/decode_tgpr_eval92 || exit 1;

# At this point you could run the command below; this gets
# results that demonstrate the basis-fMLLR adaptation (adaptation
# on small amounts of adaptation data).
#local/run_basis_fmllr.sh

steps/lmrescore.sh --cmd "$decode_cmd" data/lang_test_tgpr data/lang_test_tg \
  data/test_dev93 exp/tri4/decode_tgpr_dev93 exp/tri4/decode_tgpr_dev93_tg || exit 1;
steps/lmrescore.sh --cmd "$decode_cmd" data/lang_test_tgpr data/lang_test_tg \
  data/test_eval92 exp/tri4/decode_tgpr_eval92 exp/tri4/decode_tgpr_eval92_tg || exit 1;


# Trying the larger dictionary ("big-dict"/bd) + locally produced LM.
utils/mkgraph.sh data/lang_test_bd_tgpr exp/tri4 exp/tri4/graph_bd_tgpr || exit 1;

steps/decode_fmllr.sh --cmd "$decode_cmd" --nj 8 \
  exp/tri4/graph_bd_tgpr data/test_eval92 exp/tri4/decode_bd_tgpr_eval92 || exit 1;
steps/decode_fmllr.sh --cmd "$decode_cmd" --nj 10 \
  exp/tri4/graph_bd_tgpr data/test_dev93 exp/tri4/decode_bd_tgpr_dev93 || exit 1;

steps/lmrescore.sh --cmd "$decode_cmd" data/lang_test_bd_tgpr data/lang_test_bd_fg \
  data/test_eval92 exp/tri4/decode_bd_tgpr_eval92 exp/tri4/decode_bd_tgpr_eval92_fg \
   || exit 1;
steps/lmrescore.sh --cmd "$decode_cmd" data/lang_test_bd_tgpr data/lang_test_bd_tg \
  data/test_eval92 exp/tri4/decode_bd_tgpr_eval92 exp/tri4/decode_bd_tgpr_eval92_tg \
  || exit 1;

# The command below is commented out as we commented out the steps above
# that build the RNNLMs, so it would fail.
# local/run_rnnlms_tri4.sh

# The following two steps, which are a kind of side-branch, try mixing up
( # from the 3b system.  This is to demonstrate that script.
 steps/mixup.sh --cmd "$train_cmd" \
   20000 data/train_si84 data/lang exp/tri4 exp/tri4_20k || exit 1;
 steps/decode_fmllr.sh --cmd "$decode_cmd" --nj 10 \
   exp/tri4/graph_tgpr data/test_dev93 exp/tri4_20k/decode_tgpr_dev93  || exit 1;
)


# From tri4 system, align all si284 data.
steps/align_fmllr.sh --nj 20 --cmd "$train_cmd" \
  data/train_si284 data/lang exp/tri4 exp/tri4_ali_si284 || exit 1;

# From the tri4 system, train another SAT system (tri4a) with all the si284 data.
# We demonstrate the train_quick.sh script here.  It gives about the same
# results as the full training script (train_sat.sh), but we don't
# compare them here (there was a comparison in the s5 examples).

steps/train_quick.sh --cmd "$train_cmd" \
   4200 40000 data/train_si284 data/lang exp/tri4_ali_si284 exp/tri5 || exit 1;

(
 utils/mkgraph.sh data/lang_test_tgpr exp/tri5 exp/tri5/graph_tgpr || exit 1;
 steps/decode_fmllr.sh --nj 10 --cmd "$decode_cmd" \
   exp/tri5/graph_tgpr data/test_dev93 exp/tri5/decode_tgpr_dev93 || exit 1;
 steps/decode_fmllr.sh --nj 8 --cmd "$decode_cmd" \
  exp/tri5/graph_tgpr data/test_eval92 exp/tri5/decode_tgpr_eval92 || exit 1;

 utils/mkgraph.sh data/lang_test_bd_tgpr exp/tri5 exp/tri5/graph_bd_tgpr || exit 1;
 steps/decode_fmllr.sh --nj 10 --cmd "$decode_cmd" \
   exp/tri5/graph_bd_tgpr data/test_dev93 exp/tri5/decode_bd_tgpr_dev93 || exit 1;
 steps/decode_fmllr.sh --nj 8 --cmd "$decode_cmd" \
  exp/tri5/graph_bd_tgpr data/test_eval92 exp/tri5/decode_bd_tgpr_eval92 || exit 1;
) &

# Train and test MMI, and boosted MMI, on tri5 (LDA+MLLT+SAT on
# all the data).  Use 30 jobs.
steps/align_fmllr.sh --nj 30 --cmd "$train_cmd" \
  data/train_si284 data/lang exp/tri5 exp/tri5_ali_si284 || exit 1;

local/run_mmi_tri5.sh

#local/run_nnet_cpu.sh

## Segregated some SGMM builds into a separate file.
#local/run_sgmm.sh

# You probably want to run the sgmm2 recipe as it's generally a bit better:
local/run_sgmm2.sh

# You probably wany to run the hybrid recipe as it is complementary:
local/run_hybrid.sh


# Getting results [see RESULTS file]
# for x in exp/*/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh; done


# KWS setup. We leave it commented out by default
# $duration is the length of the search collection, in seconds
#duration=`feat-to-len scp:data/test_eval92/feats.scp  ark,t:- | awk '{x+=$2} END{print x/100;}'`
#local/generate_example_kws.sh data/test_eval92/ data/kws/
#local/kws_data_prep.sh data/lang_test_bd_tgpr/ data/test_eval92/ data/kws/
#
#steps/make_index.sh --cmd "$decode_cmd" --acwt 0.1 \
#  data/kws/ data/lang_test_bd_tgpr/ \
#  exp/tri5/decode_bd_tgpr_eval92/ \
#  exp/tri5/decode_bd_tgpr_eval92/kws
#
#steps/search_index.sh --cmd "$decode_cmd" \
#  data/kws \
#  exp/tri5/decode_bd_tgpr_eval92/kws
#
# If you want to provide the start time for each utterance, you can use the --segments
# option. In WSJ each file is an utterance, so we don't have to set the start time.
#cat exp/tri5/decode_bd_tgpr_eval92/kws/result.* | \
#  utils/write_kwslist.pl --flen=0.01 --duration=$duration \
#  --normalize=true --map-utter=data/kws/utter_map \
#  - exp/tri5/decode_bd_tgpr_eval92/kws/kwslist.xml

# # forward-backward decoding example [way to speed up decoding by decoding forward
# # and backward in time] 
# local/run_fwdbwd.sh

